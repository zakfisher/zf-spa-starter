/* eslint-disable no-console */
const chalk = require('chalk');

// Set environment
const env =
    process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || 'development';
const { parsed } = require('dotenv').config({ path: `.env.${env}` });

// Validate environment
if (!parsed) {
    console.log(
        chalk.red('Environmment'),
        `"${env}"`,
        chalk.red('not found.'),
        '\n'
    );
    process.exit();
}

// Log environment vars
console.log();
Object.keys(parsed).forEach(key => console.log(chalk.cyan(key), parsed[key]));
console.log();

module.exports = {
    siteMetadata: {
        title: `ZF SPA`,
    },
    plugins: [
        `gatsby-plugin-typescript`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        `gatsby-plugin-svgr`,
        {
            resolve: `gatsby-plugin-favicon`,
            options: {
                logo: './static/favicon.png',
                icons: {
                    android: true,
                    appleIcon: true,
                    appleStartup: true,
                    coast: false,
                    favicons: true,
                    firefox: true,
                    yandex: false,
                    windows: false,
                },
                // WebApp Manifest Configuration
                appName: null, // Inferred with your package.json
                appDescription: null,
                developerName: null,
                developerURL: null,
                dir: 'auto',
                lang: 'en-US',
                background: '#fff',
                theme_color: '#fff',
                display: 'standalone',
                orientation: 'any',
                start_url: '/?homescreen=1',
                version: '1.0',
            },
        },
        {
            resolve: `gatsby-plugin-alias-imports`,
            options: {
                alias: {
                    '@components': 'src/components',
                    '@forms': 'src/components/forms',
                    '@i18n': 'src/i18n',
                    '@layouts': 'src/components/layouts',
                    '@modals': 'src/components/modals',
                    '@pages': 'src/components/pages',
                    '@scripts': 'scripts',
                    '@services': 'src/services',
                    '@state': 'src/state',
                    '@themes': 'src/themes',
                },
                extensions: [],
            },
        },
        {
            resolve: `gatsby-plugin-intl`,
            options: {
                path: `${__dirname}/src/i18n`,
                languages: [`en-US`],
                defaultLanguage: `en-US`,
                redirect: false,
            },
        },
        `gatsby-plugin-react-helmet`,
    ],
};
