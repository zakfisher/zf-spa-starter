# ZF SPA Starter

A boilerplate SPA starter kit forked from GatsbyJS for web apps.

See [Gatsby Starter README](/docs/01_Gatsby_Starter_README.md) for more info.

---

## 📖 Table of Contents

1. [Quick Start](#-quick-start)
2. [Development](#-development)
3. [Production](#-production)
4. [Scripts](#-scripts)
5. [Documentation](#-documentation)

---

## 🚀 Quick Start

**Install Node Dependencies**
```
yarn install
```

- Always use yarn (vs npm) to keep the package installer consistent (`yarn.lock` vs `package-lock.json`).

**Serve Locally**
```
npm run serve [env]
```

> **`env`** = **`development`** (default), **`local`**, **`test`**, **`production`**.

- App should be running at <http://localhost:9000>.

---

## 💻 Development

**Start Dev Server**
```
npm run dev [env]
```

> **`env`** = **`development`** (default), **`local`**, **`test`**, **`production`**.

- App should be running at <http://localhost:8000>.

**Start Linter + Typechecker**
```
npm run watch
```

- Runs linter + type-checker on file changes
- Run standalone linter with **`npm run lint`**
- Run standalone type-checker with **`npm run type-check`**

**Watch Unit Tests**
```
npm run test watch
```

- Runs unit tests on file changes
- Run **`npm run test`** to run the test suite once (no watcher).
- Run **`npm run test coverage`** for coverage results.

**Format Code Style**
```
npm run format
```

- Runs prettier to update files according to code styleguide
- _Should_ fix most linting issues

---

## 🔨 Production

**Create a new build**
```
npm run build [env]
```

> **`env`** = **`development`** (default), **`local`**, **`test`**, **`production`**.

- Outputs a static build to **`/public`** directory.
- Serve this build using **`npm run serve`** (listed above).

---

## ⚡ Scripts

Run **`npm run help`** for information about the NPM Scripts used in this project.

---

## 📚 Documentation

To get a full understanding of the core technologies and practices used in this repo, [read the docs](/docs).
