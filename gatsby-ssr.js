import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { ServerStyleSheets } from '@material-ui/core/styles';
import { default as minifyCss } from 'minify-css-string';
import App from '@components/App';
import { Helmet } from 'react-helmet';

export const wrapRootElement = App;

export const wrapPageElement = ({ element }) => {
    const sheets = new ServerStyleSheets();
    ReactDOMServer.renderToString(sheets.collect(<App>{element}</App>));
    const css = minifyCss(sheets.toString());
    return (
        <>
            <Helmet>
                <style>{css}</style>
            </Helmet>
            {element}
        </>
    );
};
