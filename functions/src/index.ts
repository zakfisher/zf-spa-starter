// Firebase Functions w/ Typescript @ https://firebase.google.com/docs/functions/typescript
import * as functions from 'firebase-functions';
import * as CORS from 'cors';

const cors = CORS({ origin: true });

export const hello_world = functions.https.onRequest((req, res) => {
    return cors(req, res, () => {
        res.send('hello world');
    });
});
