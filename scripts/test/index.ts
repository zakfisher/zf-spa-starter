import { spawn } from 'child_process';

export default (args: string[]) => {
    const jestArgs = [];
    if (args.includes('watch')) jestArgs.push('--watch');
    if (args.includes('coverage')) jestArgs.push('--coverage');
    spawn('jest', jestArgs, { stdio: 'inherit' });
};
