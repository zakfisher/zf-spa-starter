export default {
    name: 'test',
    description: 'Run unit tests',
    usage: 'npm run test [action]',
    args: [
        {
            name: 'action',
            default: 'n/a',
            options: ['coverage', 'watch'],
        },
    ],
    examples: [
        {
            command: 'npm run test',
            description: 'Run the test suite once',
        },
        {
            command: 'npm run test watch',
            description: 'Run the test suite whenever relevant files change',
        },
        {
            command: 'npm run test coverage',
            description: 'Generate a test coverage report',
        },
    ],
};
