export default {
    name: 'serve',
    description: 'Build and serve locally at http://localhost:9000',
    usage: 'npm run serve [env]',
    args: [
        {
            name: 'env',
            default: 'development',
            options: ['development', 'local', 'test', 'production'],
        },
    ],
    examples: [
        {
            command: 'npm run serve',
            description:
                'Generate and serve a build using development environment',
        },
        {
            command: 'npm run serve local',
            description: 'Generate and serve a build using local environment',
        },
        {
            command: 'npm run serve test',
            description: 'Generate and serve a build using test environment',
        },
        {
            command: 'npm run serve production',
            description:
                'Generate and serve a build using production environment',
        },
    ],
};
