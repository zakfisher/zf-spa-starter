import { spawnSync } from 'child_process';

export default ([env = 'development']: string[]) => {
    spawnSync('gatsby', ['clean'], { stdio: 'inherit' });
    spawnSync('gatsby', ['build'], {
        stdio: 'inherit',
        env: {
            ...process.env,
            GATSBY_ACTIVE_ENV: env,
        },
    });
    spawnSync('gatsby', ['serve'], {
        stdio: 'inherit',
        env: {
            ...process.env,
            GATSBY_ACTIVE_ENV: env,
        },
    });
};
