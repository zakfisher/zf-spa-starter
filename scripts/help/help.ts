import fs from 'fs';

const SCRIPTS_DIR = __dirname.split('/help')[0];
const commands = fs
    .readdirSync(SCRIPTS_DIR)
    .filter(name => !['.', '_'].includes(name[0]));

export default {
    name: 'help',
    description: 'Show help menu for NPM scripts',
    usage: 'npm run help [command]',
    args: [
        {
            name: 'command',
            default: 'n/a',
            options: commands,
        },
    ],
    examples: [
        {
            command: 'npm run help',
            description: 'Show help menu',
        },
        {
            command: 'npm run help build',
            description: 'Show help for build command',
        },
    ],
};
