/* eslint-disable no-console */
import chalk from 'chalk';
import fs from 'fs';
const pkg = require('../../package.json');

const SCRIPTS_DIR = __dirname.split('/help')[0];

const helpers = fs
    .readdirSync(SCRIPTS_DIR)
    .filter(name => {
        const HELP_PATH = `${SCRIPTS_DIR}/${name}/help.ts`;
        return fs.existsSync(HELP_PATH);
    })
    .map(name => {
        return require(`${SCRIPTS_DIR}/${name}/help.ts`).default;
    });

const showMenu = () =>
    console.log(`
${pkg.title}
${chalk.yellow(pkg.name + ' v' + pkg.version)}

${chalk.cyan('HELP')}

    ${chalk.white(
        `Run ${chalk.green(
            `npm run help [command]`
        )} to learn more about a specific command.`
    )}

${chalk.cyan('COMMANDS')}
${helpers
    .map(
        help => `
    ${chalk.green(help.name)}
    ${help.description}
`
    )
    .join('')}`);

interface IArg {
    name: string;
    default?: string;
    options?: string[];
    example?: string;
}

interface IExample {
    command: string;
    description: string;
    args?: IArg[];
}

const showCommand = (command: string) => {
    const help = helpers.find(help => help.name === command);
    if (!help) return;

    const args = help.args.length
        ? `
    ${help.args.length ? chalk.green('Arguments') : ''}
    ${help.args
        .map(
            (arg: IArg) => `
        ${chalk.yellow(arg.name)}
        ${chalk.gray('Default')} ${arg.default}
        ${chalk.gray('Options')} ${(arg.options || []).join(', ')}
    `
        )
        .join('')}`
        : '';

    const examples = help.examples.length
        ? `
    ${help.examples.length ? chalk.green('Examples') : ''}
    ${help.examples
        .map(
            (ex: IExample) => `
        ${chalk.cyan(ex.description)}
        ${ex.command}
        ${
            ex.args
                ? ex.args
                      .map(
                          (arg: IArg) => `
            ${chalk.yellow(arg.name)}
            ${chalk.gray('Example')} ${arg.example}
            `
                      )
                      .join('')
                : ''
        }`
        )
        .join('')}`
        : '';

    console.log(`
${chalk.cyan('COMMAND HELP')}

    ${chalk.green('Command')}
    ${help.name}

    ${chalk.green('Description')}
    ${help.description}

    ${chalk.green('Usage')}
    ${help.usage}
    ${args}
    ${examples}
`);
};

export default ([command]: string[]) => {
    if (command) showCommand(command);
    else showMenu();
};
