import { spawn } from 'child_process';
import clear from 'clear';

export default () => {
    clear();
    spawn(
        'eslint',
        [
            '--ignore-path',
            '.gitignore',
            '.',
            '--ext',
            'ts',
            '--ext',
            'tsx',
            '--ext',
            'js',
            '--ext',
            'jsx',
        ],
        { stdio: 'inherit' }
    );
};
