export default {
    name: 'lint',
    description: 'Run ESLint (for TypeScript)',
    usage: 'npm run lint',
    args: [],
    examples: [],
};
