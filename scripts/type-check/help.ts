export default {
    name: 'type-check',
    description: 'Run TypeScript typechecker',
    usage: 'npm run type-check',
    args: [],
    examples: [],
};
