export default {
    name: 'clean',
    description: 'Remove cached build - /public, /.cache directories',
    usage: 'npm run clean',
    args: [],
    examples: [],
};
