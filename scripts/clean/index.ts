import { spawn } from 'child_process';

export default () => {
    spawn('gatsby', ['clean'], { stdio: 'inherit' });
};
