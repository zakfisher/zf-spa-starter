export default {
    name: 'alias',
    description: 'Show a list of alias path mappings',
    usage: 'npm run alias',
    args: [],
    examples: [
        {
            command: 'npm run alias',
            description: 'Show alias mappings',
        },
        {
            command: 'npm run help generate',
            description: 'Learn how to generate an alias',
        },
    ],
};
