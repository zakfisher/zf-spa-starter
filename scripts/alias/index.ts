/* eslint-disable no-console */
import chalk from 'chalk';

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const pkg = require(PKG_PATH);

export default () => {
    console.log(chalk.cyan('PATH ALIASES'), '\n');

    Object.keys(pkg.jest.moduleNameMapper).map(key => {
        let aliasPath = key.replace('^', '').replace('(.*)$', '');
        const relativePath = pkg.jest.moduleNameMapper[key]
            .replace('<rootDir>', '.')
            .replace('$1', '');

        while (aliasPath.length < 15) {
            aliasPath += ' ';
        }

        console.log('   ', chalk.green(aliasPath), relativePath);
    });
    console.log();
};
