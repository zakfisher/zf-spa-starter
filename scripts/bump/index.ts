/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const pkg = require(PKG_PATH);

enum ISemVer {
    PATCH = 'patch',
    MINOR = 'minor',
    MAJOR = 'major',
}

export default ([semver = ISemVer.PATCH]: ISemVer[]) => {
    let [major, minor, patch] = pkg.version
        .split('.')
        .map((v: string) => Number(v));
    switch (semver) {
        case ISemVer.PATCH:
            patch++;
            break;
        case ISemVer.MINOR:
            patch = 0;
            minor++;
            break;
        case ISemVer.MAJOR:
            patch = 0;
            minor = 0;
            major++;
            break;
    }
    pkg.version = `${major}.${minor}.${patch}`;
    fs.writeFileSync(PKG_PATH, JSON.stringify(pkg, null, 2));
    console.log(chalk.green(pkg.version));
};
