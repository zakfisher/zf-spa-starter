export default {
    name: 'bump',
    description: 'Bump the package version (alternative to npm version)',
    usage: 'npm run bump [semver]',
    args: [
        {
            name: 'semver',
            default: 'patch',
            options: ['patch', 'minor', 'major'],
        },
    ],
    examples: [
        {
            command: 'npm run bump',
            description:
                'Increment the patch version (bug fixes + enhancements)',
        },
        {
            command: 'npm run bump minor',
            description: 'Increment the minor version (new features)',
        },
        {
            command: 'npm run bump major',
            description: 'Increment the major version (breaking changes)',
        },
    ],
};
