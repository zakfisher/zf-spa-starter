export default {
    name: 'build',
    description: 'Output a static-generated build to /public',
    usage: 'npm run build [env]',
    args: [
        {
            name: 'env',
            default: 'development',
            options: ['development', 'local', 'test', 'production'],
        },
    ],
    examples: [
        {
            command: 'npm run build',
            description: 'Generate a build using development environment',
        },
        {
            command: 'npm run build local',
            description: 'Generate a build using local environment',
        },
        {
            command: 'npm run build test',
            description: 'Generate a build using test environment',
        },
        {
            command: 'npm run build production',
            description: 'Generate a build using production environment',
        },
    ],
};
