export default {
    name: 'watch',
    description: 'Run ESLint & Typechecker on file changes',
    usage: 'npm run watch',
    args: [],
    examples: [],
};
