import { spawn } from 'child_process';
export default () => {
    spawn(
        'nodemon',
        [
            '-e',
            'ts,tsx,js,jsx,json',
            '-x',
            'npm run lint clear && npm run type-check',
        ],
        { stdio: 'inherit' }
    );
};
