import { spawnSync } from 'child_process';

enum IService {
    ALL = 'all',
    FUNCTIONS = 'functions',
    HOSTING = 'hosting',
}

export default ([service = 'all']: string[]) => {
    const args = ['deploy'];

    switch (service) {
        case IService.FUNCTIONS:
            args.push('--only', 'functions');
            break;
        case IService.HOSTING:
            args.push('--only', 'hosting');
            break;
        default:
    }

    if (service !== IService.FUNCTIONS) {
        spawnSync('npm', ['run', 'build', 'production'], { stdio: 'inherit' });
    }
    spawnSync('firebase', args, { stdio: 'inherit' });
};
