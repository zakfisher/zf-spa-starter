export default {
    name: 'deploy',
    description: 'Deploy hosting and functions to Firebase',
    usage: 'npm run deploy [service]',
    args: [
        {
            name: 'service',
            default: 'all',
            options: ['all', 'functions', 'hosting'],
        },
    ],
    examples: [
        {
            command: 'npm run deploy',
            description: 'Deploy all services to Firebase',
        },
        {
            command: 'npm run deploy functions',
            description: 'Deploy functions to Firebase',
        },
        {
            command: 'npm run deploy hosting',
            description: 'Deploy hosting to Firebase',
        },
    ],
};
