import { spawn } from 'child_process';

export default ([env = 'development']: string[]) => {
    spawn('gatsby', ['develop'], {
        stdio: 'inherit',
        env: {
            ...process.env,
            GATSBY_ACTIVE_ENV: env,
        },
    });
};
