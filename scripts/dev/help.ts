export default {
    name: 'dev',
    description: 'Run the Gatsby development server at http://localhost:8000',
    usage: 'npm run dev [env]',
    args: [
        {
            name: 'env',
            default: 'development',
            options: ['development', 'local', 'test', 'production'],
        },
    ],
    examples: [
        {
            command: 'npm run dev',
            description: 'Run dev server with development environment',
        },
        {
            command: 'npm run dev local',
            description: 'Run dev server with local environment',
        },
        {
            command: 'npm run dev test',
            description: 'Run dev server with test environment',
        },
        {
            command: 'npm run dev production',
            description: 'Run dev server with production environment',
        },
    ],
};
