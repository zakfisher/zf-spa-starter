/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';

const args = process.argv.slice(2);
const script = args.shift();
const SCRIPT_PATH = script ? `${__dirname}/${script}` : null;
if (!SCRIPT_PATH || !fs.existsSync(SCRIPT_PATH)) {
    console.log(chalk.red(`Script "${script}" not found.`), '\n');
    process.exit();
}

const exec = require(SCRIPT_PATH).default;
exec(args);
