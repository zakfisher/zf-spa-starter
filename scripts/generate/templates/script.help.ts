export default (scriptName: string) => `\
export default {
    name: '${scriptName}',
    description: '...',
    usage: 'npm run ${scriptName}',
    args: [],
    examples: [],
};
`;
