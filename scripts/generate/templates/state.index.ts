export default (sliceName: string) => `\
import { createSlice } from '@reduxjs/toolkit';
import { IThunkAction } from '@state/types';
import * as api from '@services/api';

export const defaultState = {
    data: [],
    fetching: false,
    fetchSuccess: false,
    fetchError: null,
};

const slice = createSlice({
    name: '${sliceName}',
    initialState: defaultState,
    reducers: {
        fetchRequest(state) {
            state.fetching = true;
            state.fetchSuccess = false;
            state.fetchError = null;
        },
        fetchSuccess(state, action) {
            state.data = action.payload;
            state.fetching = false;
            state.fetchSuccess = true;
            state.fetchError = null;
        },
        fetchError(state, action) {
            state.fetching = false;
            state.fetchSuccess = false;
            state.fetchError = action.payload;
        },
    },
});

export const { fetchRequest, fetchSuccess, fetchError } = slice.actions;
export default slice.reducer;

export const fetch = (): IThunkAction => async (dispatch, getState) => {
    const state = getState();

    // Check for current request
    if (state.${sliceName}.fetching) return;

    // Check for previous request
    if (state.${sliceName}.fetchSuccess) return;
    if (state.${sliceName}.fetchError) return;

    // Issue new request
    dispatch(fetchRequest());
    try {
        const res = await api.getUsers();
        dispatch(fetchSuccess(res.data));
    } catch (e) {
        dispatch(fetchError(e.message));
    }
};
`;
