export default (sliceName: string) => `\
import { store } from '@services/test/setup';
import reducer, {
    defaultState,
    fetchRequest,
    fetchSuccess,
    fetchError,
    fetch,
} from '.';

describe('${sliceName}', () => {
    describe('defaultState', () => {
        it('should have expected data', () => {
            expect(defaultState.data).toEqual([]);
            expect(defaultState.fetching).toEqual(false);
            expect(defaultState.fetchSuccess).toEqual(false);
            expect(defaultState.fetchError).toEqual(null);
        });
    });
    describe('actions', () => {
        describe('fetchRequest', () => {
            it('should dispatch the correct action', () => {
                expect(store.getActions()).toHaveLength(0);
                store.dispatch(fetchRequest());
                expect(store.getActions()[0].type).toBe('${sliceName}/fetchRequest');
            });
        });
        describe('fetchSuccess', () => {
            it('should dispatch the correct action', () => {
                expect(store.getActions()).toHaveLength(0);
                const payload = { data: [{ id: '123' }] };
                store.dispatch(fetchSuccess(payload));
                expect(store.getActions()[0].type).toBe('${sliceName}/fetchSuccess');
                expect(store.getActions()[0].payload).toEqual(payload);
            });
        });
        describe('fetchError', () => {
            it('should dispatch the correct action', () => {
                expect(store.getActions()).toHaveLength(0);
                store.dispatch(fetchError({ message: 'Nope!' } as Error));
                expect(store.getActions()[0].type).toBe('${sliceName}/fetchError');
            });
        });
        describe('fetch', () => {
            it('should dispatch fetchRequest action', () => {
                expect(store.getActions()).toHaveLength(0);
                store.dispatch(fetch() as any);
                expect(store.getActions()[0].type).toBe('${sliceName}/fetchRequest');
            });
        });
    });
    describe('state', () => {
        describe('data', () => {
            it('should return the correct value', () => {
                expect(reducer(defaultState, { type: 'any' }).data).toEqual(
                    defaultState.data
                );
                const payload = { data: [{ id: '123' }] };
                expect(
                    reducer(defaultState, {
                        type: '${sliceName}/fetchSuccess',
                        payload,
                    }).data
                ).toEqual(payload);
            });
        });
        describe('fetching', () => {
            it('should return the correct value', () => {
                expect(reducer(defaultState, { type: 'any' }).fetching).toEqual(
                    defaultState.fetching
                );
                expect(
                    reducer(defaultState, {
                        type: '${sliceName}/fetchRequest',
                    }).fetching
                ).toEqual(true);
            });
        });
        describe('fetchSuccess', () => {
            it('should return the correct value', () => {
                expect(
                    reducer(defaultState, { type: 'any' }).fetchSuccess
                ).toEqual(defaultState.fetchSuccess);
                expect(
                    reducer(defaultState, {
                        type: '${sliceName}/fetchSuccess',
                    }).fetchSuccess
                ).toEqual(true);
            });
        });
        describe('fetchError', () => {
            it('should return the correct value', () => {
                expect(
                    reducer(defaultState, { type: 'any' }).fetchError
                ).toEqual(defaultState.fetchError);
                const payload = { message: 'Oopsy' } as Error;
                expect(
                    reducer(defaultState, {
                        type: '${sliceName}/fetchError',
                        payload,
                    }).fetchError
                ).toEqual(payload);
            });
        });
    });
});
`;
