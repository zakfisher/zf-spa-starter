export default (componentName: string) => `\
import React from 'react';

import { render } from '@services/test/setup';
import ${componentName} from '..';

describe('${componentName}', () => {
    it('renders correctly', () => {
        const { asFragment } = render(<${componentName} />);
        expect(asFragment()).toMatchSnapshot();
    });
});
`;
