export default (componentName: string) => `\
import React from 'react';
import { connect } from 'react-redux';
import { useIntl } from 'gatsby-plugin-intl';
// import { IRootState } from '@state/types';
import {
    GKForm,
    GKFormGroup,
    GKLabel,
    GKInput,
    GKButton,
    Row,
    Col,
} from '@gkernel/ux-components';

interface IProps {
    submit: (value: string) => void;
}

const ${componentName} = (props: IProps) => {
    const intl = useIntl();
    const [value, setValue] = React.useState('');

    const onSubmit = React.useCallback(() => {
        props.submit(value);
    }, [value]);

    const onValueChange = React.useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            setValue(e.target.value);
        },
        [value]
    );

    return (
        <GKForm>
            <Row className="d-flex justify-content-center">
                <Col xs={12} md={7}>
                    <GKFormGroup>
                        <GKLabel for="value">
                            {intl.formatMessage({ id: 'value' })}
                        </GKLabel>
                        <GKInput
                            type="text"
                            id="value"
                            value={value}
                            onChange={onValueChange}
                        />
                    </GKFormGroup>
                    <GKFormGroup>
                        <GKButton color="primary" onClick={onSubmit}>
                            {intl.formatMessage({ id: 'submit' })}
                        </GKButton>
                    </GKFormGroup>
                </Col>
            </Row>
        </GKForm>
    );
};
${componentName}.displayName = '${componentName}';

const mapState = () => ({});

// import { logInWithCredentials } from '@state/auth';
// const mapDispatch = { logInWithCredentials };

const mapDispatch = {
    submit: (value: string) => {
        console.log('submit', value); // eslint-disable-line
    },
};

export default connect(mapState, mapDispatch)(${componentName});
`;
