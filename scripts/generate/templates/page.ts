export default (componentName: string) => `\
import ${componentName} from '@pages/${componentName}';
export default ${componentName};
`;
