export default (componentName: string) => `\
import React from 'react';
const ${componentName} = () => <div>Layout: ${componentName}</div>;
${componentName}.displayName = '${componentName}';
export default ${componentName};
`;
