export default (componentName: string) => `\
import React from 'react';
import Page, { IProps as IPageProps } from '@pages/Page';

const pageProps = {
    title: '${componentName}',
    access: {
        public: true,
    },
} as IPageProps;

const ${componentName} = () => <Page {...pageProps}>${componentName}</Page>;
${componentName}.displayName = '${componentName}';

export default ${componentName};

`;
