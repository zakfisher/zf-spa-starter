export default () => `\
export interface IData {
    id?: string;
}

export interface IState {
    data: IData[];
    fetching: boolean;
    fetchSuccess: boolean;
    fetchError?: string;
}
`;
