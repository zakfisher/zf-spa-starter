export default (componentName: string) => `\
import React from 'react';
const ${componentName} = () => <div>Modal: ${componentName}</div>;
${componentName}.displayName = '${componentName}';
export default ${componentName};
`;
