/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';
import { spawnSync } from 'child_process';
import generateComponent from './generate-component';
import gatsbyPageTemplate from './templates/page';

const ROOT_DIR = __dirname.split('/scripts')[0];
const GATSBY_PAGES_DIR = `${ROOT_DIR}/src/pages`;

export default ([route, componentName]: string[]) => {
    if (route[0] !== '/') route = '/' + route;

    const GATSBY_PAGE_DIR = `${GATSBY_PAGES_DIR}${route}${
        route.split('').pop() === '/' ? '' : '/'
    }`;
    const GATSBY_PAGE = `${GATSBY_PAGE_DIR}index.tsx`;

    if (fs.existsSync(GATSBY_PAGE)) {
        console.log(chalk.red(`Route "${route}" is taken.`));
        console.log(
            chalk.red(
                `If you want to use it, run ${chalk.white(
                    `rm -rf ${GATSBY_PAGE}`
                )} and try again.`
            )
        );
        return;
    }

    generateComponent(['page', componentName]);

    if (!componentName) return;

    if (!fs.existsSync(GATSBY_PAGE_DIR)) {
        spawnSync('mkdir', [GATSBY_PAGE_DIR], { stdio: 'inherit' });
        console.log(
            chalk.gray('New Folder'),
            GATSBY_PAGE_DIR.replace(ROOT_DIR, '')
        );
    }
    fs.writeFileSync(GATSBY_PAGE, gatsbyPageTemplate(componentName));
    console.log(
        chalk.gray('New File  '),
        GATSBY_PAGE.replace(ROOT_DIR, ''),
        '\n'
    );

    console.log(
        `View page at ${chalk.cyan(`http://localhost:8000${route}`)}`,
        '\n'
    );
};
