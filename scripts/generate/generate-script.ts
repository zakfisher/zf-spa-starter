/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';
import { spawnSync } from 'child_process';

import indexTemplate from './templates/script.index';
import helpTemplate from './templates/script.help';

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const SCRIPTS_DIR = `${ROOT_DIR}/scripts`;
const pkg = require(PKG_PATH);

export default ([scriptName]: string[]) => {
    if (!scriptName) {
        return console.log(chalk.red('A script name is required.'));
    }

    if (scriptName === 'exec') {
        return console.log(chalk.red('Script name "exec" is reserved.'));
    }

    if (scriptName.includes(' ')) {
        return console.log(chalk.red(`Script name cannot contain spaces.`));
    }

    const SCRIPT_DIR = `${SCRIPTS_DIR}/${scriptName}`;
    const SCRIPT_INDEX = `${SCRIPT_DIR}/index.ts`;
    const SCRIPT_HELP = `${SCRIPT_DIR}/help.ts`;

    if (fs.existsSync(SCRIPT_INDEX)) {
        return console.log(chalk.red(`Script "${scriptName}" already exists.`));
    }

    // Create folder + files
    spawnSync('mkdir', [SCRIPT_DIR], { stdio: 'inherit' });
    console.log(chalk.gray('New Folder'), SCRIPT_DIR.replace(ROOT_DIR, ''));
    fs.writeFileSync(SCRIPT_INDEX, indexTemplate(scriptName));
    console.log(chalk.gray('New File  '), SCRIPT_INDEX.replace(ROOT_DIR, ''));
    fs.writeFileSync(SCRIPT_HELP, helpTemplate(scriptName));
    console.log(
        chalk.gray('New File  '),
        SCRIPT_HELP.replace(ROOT_DIR, ''),
        '\n'
    );

    // Add script to package.json
    pkg.scripts[scriptName] = `npm run --silent exec ${scriptName}`;
    const pkgSort = Object.keys(pkg.scripts).sort();
    const sortedScripts: { [key: string]: string } = {};
    pkgSort.map(key => (sortedScripts[key] = pkg.scripts[key]));
    pkg.scripts = sortedScripts;
    fs.writeFileSync(PKG_PATH, JSON.stringify(pkg, null, 2));

    console.log(chalk.gray('Added script to package.json'), '\n');

    console.log(
        `Run ${chalk.cyan(`npm run ${scriptName}`)} to run this script.`,
        '\n'
    );
    console.log(
        `Run ${chalk.cyan(`npm run help ${scriptName}`)} for help.`,
        '\n'
    );
};
