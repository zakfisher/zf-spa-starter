/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';
import { spawnSync } from 'child_process';

import formTemplate from './templates/component.form';
import layoutTemplate from './templates/component.layout';
import modalTemplate from './templates/component.modal';
import pageTemplate from './templates/component.page';
import testTemplate from './templates/component.test';

const templates: {
    [alias: string]: (componentName: string) => string;
} = {
    '@forms': formTemplate,
    '@layouts': layoutTemplate,
    '@modals': modalTemplate,
    '@pages': pageTemplate,
};

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const pkg = require(PKG_PATH);

const generate = (alias: string, componentName: string) => {
    if (!componentName) {
        return console.log(chalk.red(`A component name is required.`));
    }

    if (!templates[alias]) {
        return console.log(chalk.red(`No template found for alias ${alias}.`));
    }

    const PARENT_DIR = pkg.jest.moduleNameMapper[`^${alias}(.*)$`]
        .replace('<rootDir>', ROOT_DIR)
        .replace('$1', '');
    const DEST = `${PARENT_DIR}/${componentName}`;
    const COMPONENT = `${DEST}/index.tsx`;
    const TEST_DIR = `${DEST}/__tests__`;
    const TEST = `${TEST_DIR}/test.tsx`;

    if (fs.existsSync(DEST)) {
        return console.log(
            chalk.red(`A component named ${componentName} already exists`)
        );
    }

    // Create folders/files
    if (!fs.existsSync(PARENT_DIR)) {
        spawnSync('mkdir', [PARENT_DIR], { stdio: 'inherit' });
        console.log(
            chalk.gray('Created directory'),
            PARENT_DIR.replace(ROOT_DIR, '')
        );
    }

    spawnSync('mkdir', [DEST], { stdio: 'inherit' });
    console.log(chalk.gray('New Folder'), DEST.replace(ROOT_DIR, ''));
    fs.writeFileSync(COMPONENT, templates[alias](componentName));
    console.log(chalk.gray('New File  '), COMPONENT.replace(ROOT_DIR, ''));

    spawnSync('mkdir', [TEST_DIR], { stdio: 'inherit' });
    console.log(chalk.gray('New Folder'), TEST_DIR.replace(ROOT_DIR, ''));
    fs.writeFileSync(TEST, testTemplate(componentName));
    console.log(chalk.gray('New File  '), TEST.replace(ROOT_DIR, ''), '\n');

    console.log(
        `import ${componentName} from '${alias}/${componentName}'`,
        '\n'
    );
};

export default ([type, componentName]: string[]) => {
    if (type === 'form') return generate('@forms', componentName);
    if (type === 'layout') return generate('@layouts', componentName);
    if (type === 'modal') return generate('@modals', componentName);
    if (type === 'page') return generate('@pages', componentName);
};
