export default {
    name: 'generate',
    description: 'Generate new files for various parts of the project',
    usage: 'npm run generate [type]',
    args: [
        {
            name: 'type',
            default: 'n/a',
            options: [
                'alias',
                'form',
                'layout',
                'modal',
                'page',
                'script',
                'state',
            ],
        },
    ],
    examples: [
        {
            command: 'npm run generate alias [aliasPath] [relativePath]',
            description:
                'Generate an alias path (adds mappings for Gatsby, Jest, TypeScript)',
            args: [
                {
                    name: 'aliasPath',
                    example: '@pages',
                },
                {
                    name: 'relativePath',
                    example: './src/pages',
                },
            ],
        },
        {
            command: 'npm run generate form [componentName]',
            description: 'Generate a new form component',
            args: [
                {
                    name: 'componentName',
                    example: 'MyForm',
                },
            ],
        },
        {
            command: 'npm run generate layout [componentName]',
            description: 'Generate a new layout component',
            args: [
                {
                    name: 'componentName',
                    example: 'MyLayout',
                },
            ],
        },
        {
            command: 'npm run generate modal [componentName]',
            description: 'Generate a new modal component',
            args: [
                {
                    name: 'componentName',
                    example: 'MyModal',
                },
            ],
        },
        {
            command: 'npm run generate page [route] [componentName]',
            description: 'Generate a new page component',
            args: [
                {
                    name: 'route',
                    example: '/',
                },
                {
                    name: 'componentName',
                    example: 'HomePage',
                },
            ],
        },
        {
            command: 'npm run generate script [scriptName]',
            description: 'Generate a new NPM script',
            args: [
                {
                    name: 'scriptName',
                    example: 'awesome-script',
                },
            ],
        },
        {
            command: 'npm run generate state',
            description:
                'Re-write the Redux store root reducer manually (helpful when deleting slices)',
        },
        {
            command: 'npm run generate state [sliceName]',
            description: 'Generate a new Redux state slice',
            args: [
                {
                    name: 'sliceName',
                    example: 'tasks',
                },
            ],
        },
    ],
};
