/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';
import { spawnSync } from 'child_process';

import indexTemplate from './templates/state.index';
import testTemplate from './templates/state.test';
import typesTemplate from './templates/state.types';
import rootReducerTemplate from './templates/store.reducer';

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const pkg = require(PKG_PATH);
const STATE_DIR = pkg.jest.moduleNameMapper[`^@state(.*)$`]
    .replace('<rootDir>', ROOT_DIR)
    .replace('$1', '');
const ROOT_REDUCER = `${STATE_DIR}/reducer.ts`;

export default ([sliceName]: string[]) => {
    if (sliceName) {
        const SLICE_DIR = `${STATE_DIR}/${sliceName}`;
        const SLICE = `${SLICE_DIR}/index.ts`;
        const TEST = `${SLICE_DIR}/test.ts`;
        const TYPES = `${SLICE_DIR}/types.ts`;

        if (fs.existsSync(SLICE)) {
            return console.log(
                chalk.red(`A slice named "${sliceName}" already exists.`)
            );
        }

        // Create folders/files
        spawnSync('mkdir', [SLICE_DIR], { stdio: 'inherit' });
        console.log(chalk.gray('New Folder'), SLICE_DIR.replace(ROOT_DIR, ''));
        fs.writeFileSync(SLICE, indexTemplate(sliceName));
        console.log(chalk.gray('New File  '), SLICE.replace(ROOT_DIR, ''));
        fs.writeFileSync(TEST, testTemplate(sliceName));
        console.log(chalk.gray('New File  '), TEST.replace(ROOT_DIR, ''));
        fs.writeFileSync(TYPES, typesTemplate());
        console.log(chalk.gray('New File  '), TYPES.replace(ROOT_DIR, ''));
    }

    const slices = fs.readdirSync(STATE_DIR).filter(name => {
        return (
            name[0] !== '_' &&
            fs.lstatSync(`${STATE_DIR}/${name}`).isDirectory()
        );
    });

    // Re-write root reducer, regardless of new slice
    fs.writeFileSync(ROOT_REDUCER, rootReducerTemplate(slices));
    console.log(
        chalk.gray('New File  '),
        ROOT_REDUCER.replace(ROOT_DIR, ''),
        '\n'
    );
};
