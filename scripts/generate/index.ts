import generateAlias from './generate-alias';
import generateComponent from './generate-component';
import generatePage from './generate-page';
import generateScript from './generate-script';
import generateState from './generate-state';

enum IType {
    ALIAS = 'alias',
    FORM = 'form',
    LAYOUT = 'layout',
    MODAL = 'modal',
    PAGE = 'page',
    SCRIPT = 'script',
    STATE = 'state',
}

export default (args: string[]) => {
    switch (args[0]) {
        case IType.ALIAS:
            return generateAlias(args.slice(1));
        case IType.FORM:
        case IType.LAYOUT:
        case IType.MODAL:
            return generateComponent(args);
        case IType.PAGE:
            return generatePage(args.slice(1));
        case IType.SCRIPT:
            return generateScript(args.slice(1));
        case IType.STATE:
            return generateState(args.slice(1));
        default:
    }
};
