/* eslint-disable no-console */
import fs from 'fs';
import chalk from 'chalk';

const ROOT_DIR = __dirname.split('/scripts')[0];
const PKG_PATH = `${ROOT_DIR}/package.json`;
const TS_CONFIG = `${ROOT_DIR}/tsconfig.json`;
const GATSBY_CONFIG = `${ROOT_DIR}/gatsby-config.js`;

const updateGatsbyMappings = (aliasPath: string, relativePath: string) => {
    let config = fs.readFileSync(GATSBY_CONFIG, 'utf-8').split('\n');

    const pathLine =
        config.find(line => line.includes('gatsby-plugin-alias-imports')) || '';
    const firstPathIndex = 3 + config.indexOf(pathLine);

    const existingLine = config.find(line => line.includes(aliasPath)) || '';
    const line = `                    '${aliasPath}': '${relativePath.substr(
        2
    )}',`;

    let insertIndex =
        existingLine.trim().length > 0 ? config.indexOf(existingLine) : -1;
    if (insertIndex < 0) {
        insertIndex = firstPathIndex;
        config = config
            .slice(0, insertIndex)
            .concat(line)
            .concat(config.slice(insertIndex));
    } else {
        config[insertIndex] = line;
    }

    let lastPathIndex = firstPathIndex;
    while (!config[lastPathIndex].includes('}')) {
        lastPathIndex++;
    }

    const sortedLines: string[] = config
        .slice(firstPathIndex, lastPathIndex)
        .sort();
    sortedLines.map((line, i) => {
        config[i + firstPathIndex] = `${line.replace(`',`, `'`)},`;
    });

    fs.writeFileSync(GATSBY_CONFIG, config.join('\n'));
    console.log(
        chalk.gray(
            `Updated Gatsby Mappings ${GATSBY_CONFIG.replace(ROOT_DIR, '')}`
        )
    );
};

const updateTypeScriptMappings = (aliasPath: string, relativePath: string) => {
    let config = fs.readFileSync(TS_CONFIG, 'utf-8').split('\n');

    const pathLine = config.find(line => line.includes(`"paths": {`)) || '';
    const firstPathIndex = 1 + config.indexOf(pathLine);

    const existingLine = config.find(line => line.includes(aliasPath)) || '';
    const line = `        "${aliasPath}/*": ["${relativePath.substr(2)}/*"],`;

    let insertIndex =
        existingLine.trim().length > 0 ? config.indexOf(existingLine) : -1;
    if (insertIndex < 0) {
        insertIndex = firstPathIndex;
        config = config
            .slice(0, insertIndex)
            .concat(line)
            .concat(config.slice(insertIndex));
    } else {
        config[insertIndex] = line;
    }

    let lastPathIndex = firstPathIndex;
    while (!config[lastPathIndex].includes('}')) {
        lastPathIndex++;
    }

    const sortedLines: string[] = config
        .slice(firstPathIndex, lastPathIndex)
        .sort();
    sortedLines.map((line, i) => {
        config[i + firstPathIndex] = `${line.replace(`],`, `]`)}${
            i < sortedLines.length - 1 ? ',' : ''
        }`;
    });

    fs.writeFileSync(TS_CONFIG, config.join('\n'));
    console.log(
        chalk.gray(`Updated Gatsby Mappings ${TS_CONFIG.replace(ROOT_DIR, '')}`)
    );
};

const updateJestMappings = (aliasPath: string, relativePath: string) => {
    const pkg = require(PKG_PATH);
    pkg.jest.moduleNameMapper[
        `^${aliasPath}(.*)$`
    ] = `<rootDir>${relativePath.substr(1)}$1`;
    const pkgSort = Object.keys(pkg.jest.moduleNameMapper).sort();
    const sortedPkgAliases: { [key: string]: string } = {};
    pkgSort.map(
        key => (sortedPkgAliases[key] = pkg.jest.moduleNameMapper[key])
    );
    pkg.jest.moduleNameMapper = sortedPkgAliases;
    fs.writeFileSync(PKG_PATH, JSON.stringify(pkg, null, 2));
    console.log(
        chalk.gray(`Updated Jest Mappings ${PKG_PATH.replace(ROOT_DIR, '')}`)
    );
};

export default ([aliasPath, relativePath]: string[]) => {
    if (!aliasPath || !relativePath) return;
    if (aliasPath[0] !== '@')
        return console.log(chalk.red('Alias path must begin with "@"'));
    if (relativePath.substr(0, 2) !== './')
        return console.log(chalk.red('Relative path must begin with "./"'));

    updateGatsbyMappings(aliasPath, relativePath);
    updateTypeScriptMappings(aliasPath, relativePath);
    updateJestMappings(aliasPath, relativePath);

    console.log();
    console.log(chalk.green('Generated new alias'), '\n');
    console.log(`+ import ___ from "${aliasPath}/___"`);
    console.log(chalk.gray(`- import ___ from "${relativePath}/___"`), '\n');
};
