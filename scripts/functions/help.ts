export default {
    name: 'functions',
    description: 'Run a Firebase Functions script',
    usage: 'npm run functions [script]',
    args: [
        {
            name: 'script',
            default: 'n/a',
            options: [
                'build',
                'deploy',
                'lint',
                'logs',
                'serve',
                'shell',
                'start',
            ],
        },
    ],
    examples: [
        {
            command: 'npm run functions build',
            description: 'Compile TypeScript (outputs to ./functions/lib)',
        },
        {
            command: 'npm run functions deploy',
            description: 'Deploy functions to Firebase',
        },
        {
            command: 'npm run functions lint',
            description: 'Run TypeScript type-checker against functions',
        },
        {
            command: 'npm run functions logs',
            description: 'View logs',
        },
        {
            command: 'npm run functions serve',
            description: 'Serve functions locally (dev)',
        },
        {
            command: 'npm run functions shell',
            description: 'Serve functions in production',
        },
        {
            command: 'npm run functions start',
            description: 'Serve functions in production',
        },
    ],
};
