import { spawnSync } from 'child_process';

export default ([script]: string[]) => {
    if (!script) return;
    spawnSync('npm', ['--prefix', 'functions', 'run', script], {
        stdio: 'inherit',
    });
};
