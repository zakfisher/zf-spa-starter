export default {
    name: 'format',
    description: 'Format code according to prettier-defined code styles',
    usage: 'npm run format',
    args: [],
    examples: [],
};
