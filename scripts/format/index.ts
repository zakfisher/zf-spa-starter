import { spawn } from 'child_process';
export default () => {
    spawn(
        'prettier',
        ['--write', '**/*.{js,jsx,ts,tsx}', '--loglevel', 'log'],
        { stdio: 'inherit' }
    );
};
