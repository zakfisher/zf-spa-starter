/* Gatsby Browser API @ https://www.gatsbyjs.org/docs/browser-apis */
import App from '@components/App';
import store from '@state/store';
import { initApp } from '@state/app';

export const wrapRootElement = App;

export const onClientEntry = () => store.dispatch(initApp());

/* eslint-disable */
export const onRouteUpdate = ({ location, prevLocation }) => {
    console.log('new pathname', location.pathname);
    console.log('old pathname', prevLocation ? prevLocation.pathname : null);

    // Validate that current user has access to the new page
    // const canView = window.location.pathname === '/';

    // console.log('canView', canView);
    // console.log('state', store.getState());

    // If not, redirect
    // if (!canView) window.location.href = '/';
};
