## Routing

Gatsby builds multiple HTML files that behave like a single page application (SPA). The first page is rendered on the server, then all subsequent pages are rendered on the client.
Navigating between pages on the client requires using Gatsby's built-in router, which is built on [@reach/router](https://www.gatsbyjs.org/docs/reach-router-and-gatsby/).

- See [How Gatsby Handles Routing](https://www.gatsbyjs.org/docs/routing/)


___


#### Contents

1. [Page Folder Structure](#page-folder-structure)
2. [Navigating with Link Component](#navigating-with-link-component)
3. [Navigating Programmatically](#navigating-programmatically)
4. [In-Route Navigation](#in-route-navigation)


---


#### Page Folder Structure

Gatsby automatically determines routes from the **`./src/pages`** directory.

```
.
└── src
    └── pages
        ├── about
        |   └── index.tsx
        └── index.tsx
```

The page component definitions _can_ live in this directory, but this repo abstracts them for better separation of concerns, so these files look like:

**`./src/pages/index.tsx`** » **`/`**
```
import HomePage from '@pages/HomePage';
export default HomePage;
```

**`./src/pages/about/index.tsx`** » **`/about`**
```
import AboutPage from '@pages/AboutPage';
export default AboutPage;
```

The **`@pages`** path alias defaults to **`./src/components/pages`**.
```
.
└── src
    └── components
        └── pages
            ├── AboutPage
            |   └── index.tsx
            └── HomePage
                └── index.tsx

```


---


#### Navigating with Link Component

The **`Link`** component from **`gatsby`** tells Gatsby which page to render dynamically, based on the route.

- See [Gatsby Link Component API](https://www.gatsbyjs.org/docs/gatsby-link/)

To enable routing for multiple i18n locales, we use **`Link`** from **`gatby-plugin-intl`**, which wraps the default Gatsby **`Link`** and persists the current locale by automatically prefixing our urls with it (ex: **`/login`** » **`/en-US/login`**).

- See [Gatsby i18n Plugin](https://www.gatsbyjs.org/packages/gatsby-plugin-intl/)

**Basic Example**
```
import React from 'react';
import { Link } from 'gatsby-plugin-intl';

# @pages/HomePage
const HomePage = () => (
    <div>
      <Link to="/about">About</Link>
    </div>
);
```

**Advanced Example (passing state as props)**
```
import React from 'react';
import { Link } from 'gatsby-plugin-intl';

# @pages/HomePage
const HomePage = () => (
    <div>
      <Link
        to="/about"
        state={{ data: true }}
    >About</Link>
    </div>
);

# @pages/AboutPage
const AboutPage = (props) => {
    console.log(props.location.data); // true
    return <div />;
};
```

---


#### Navigating Programmatically

**Basic Example (no data)**
```
import React from 'react';
import { navigate } from 'gatsby';

# @pages/HomePage
const HomePage = () => (
    <div>
        <button onClick={() => {
            navigate('/about');
        }}>Click Me!</button>
    </div>
);
```

**Advanced Example (passing state as props)**
```
import React from 'react';
import { navigate } from 'gatsby';

# @pages/HomePage
const HomePage = () => (
    <div>
        <button onClick={() => {
            navigate('/about', {
                state: { data: true }
            });
        }}>Click Me!</button>
    </div>
);

# @pages/AboutPage
const AboutPage = (props) => {
    console.log(props.location.data); // true
    return <div />;
};
```


---


#### In-Route Navigation

Neither **`<Link>`** nor **`navigate`** can be used for in-route navigation with a hash or query parameter.
If you need this behavior, you should either use an anchor tag or import the **`@reach/router`** package—which Gatsby already depends upon—to make use of its navigate function, like so:

**Basic Example (no data)**
```
import React from 'react';
import { navigate } from '@reach/router';

# @pages/HomePage
const HomePage = () => (
    <div>
        <button onClick={() => {
            navigate('#some-link');
            // OR
            navigate('?foo=bar');
        }}>Click Me!</button>
    </div>
);
```
