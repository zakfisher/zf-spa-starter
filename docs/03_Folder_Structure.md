## Folder Structure

This repo comes with a default folder structure, but [Path Aliases](/docs/07_Path_Aliases.md) make it easy to rearrange things however make sense.

```
.
├── .env.*                         # Environment Variables
|
├── .eslintrc.js                   # ESLint Configuration
├── .prettierrc                    # Prettier Configuration
├── tsconfig.json                  # TypeScript Configuration
|
├── gatsby-browser.js              # Gatsby configuration affecting browser only
├── gatsby-config.js               # Main configuration file for Gatsby (extends built-in webpack config)
├── gatsby-node.js                 # Gatsby configuration affecting the build process
├── gatsby-ssr.js                  # Gatsby configuration affecting server-side rendering
|
├── /docs                          # Documentation for tools/practices in this repo
|   └── <order>_<title>.md         # Document provided as markdown
|
├── /public                        # Gatsby build folder
|
├── /scripts                       # Folder containing ALL NPM scripts (allowing TypeScript via ts-node)
|   ├── _exec.ts                   # The entry point for all NPM scripts - validates script and passes command args
|   └── <script>                   # Script name should match definition in package.json
|       ├── help.ts                # Command help file, providing examples, etc for `npm run help`
|       └── index.ts               # Command definition file, ALWAYS exports a function receiving an array of strings as one argument
|
├── /src                           # Folder containing all app code
|   ├── components                 # Folder containing all React components
|   |   └── <type>                 # Components are organized by type (page, layout, modal, form, etc)
|   |       └── <Component>        # ComponentName naming convention - folder containing a single component
|   |           ├── __tests__      # Folder containing unit test suite(s)
|   |           |   └── test.tsx   # Test suite for this component
|   |           └── index.tsx      # Component definition file (may or may not use Redux#connect)
|   |
|   ├── i18n                       # Folder containing internationalization messages
|   |   └── <locale>.json          # ONE file per locale, containing ALL i18n messages
|   |
|   ├── pages                      # Gatsby-specific folder for automatic route-to-component page mapping
|   |   ├── index.tsx              # Will route to /
|   |   └── <subdirectory>         # Will route to /<subdirectory>/
|   |       └── index.tsx
|   |
|   ├── services                   # Folder containing ALL app services
|   |   └── <service>              # Service directory (api service, test service, unit conversion service)
|   |       └── index.tsx          # Whatever files make sense for this service
|   |
|   └── state                      # Folder containing SINGLE Redux store
|       └── __mocks__              # Folder containing mocks for unit tests
|           └── store.ts           # Mock store definition file
|       └── reducer.ts             # The store's root reducer (file generated automatically with `npm run generate state`)
|       └── store.ts               # Store configuration file
|       └── types.ts               # Redux type definitions
|       └── <slice>                # Folder containing files for this slice of the global state
|           └── index.ts           # Slice definition file - defines state, actions, & reducers for this slice
|           └── test.ts            # Test suite for this slice
|           └── types.ts           # Type definitions for this slice
|
├── /static                        # Folder for static (public) assets
|
└── /types                         # Folder for manual type declarations (in case @types/<module> doesn't exist)
```
