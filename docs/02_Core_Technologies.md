## Core Technologies

#### [Gatsby](https://www.gatsbyjs.org/)
> A static site generator for React

- Comes with webpack dev/build configurations built-in
- Easy to customize with a wide range of [open source plugins](https://www.gatsbyjs.org/plugins/)
- Officially recommended by [React](https://reactjs.org/docs/create-a-new-react-app.html)

#### [TypeScript](https://www.typescriptlang.org/)
> A typed superset of JavaScript that compiles to plain JavaScript

#### [React](https://reactjs.org/)
> A JavaScript library for building user interfaces

#### [Redux Toolkit](https://redux-toolkit.js.org/)
> The official, opinionated, batteries-included toolset for efficient Redux development

- Comes with powerful middleware (like [Redux Thunk](https://github.com/reduxjs/redux-thunk)) and tooling (like [Immer](https://immerjs.github.io/immer/docs/introduction)) built-in
- Allows devs to focus on managing actions and state with minimal code
- Officially recommended by [Redux](https://redux.js.org/)
- Read more [here](https://redux.js.org/redux-toolkit/overview/)

#### [Jest](https://jestjs.io/)
> Jest is a delightful JavaScript Testing Framework with a focus on simplicity

#### [React Testing Library](https://testing-library.com/)
> Simple and complete testing utilities that encourage good testing practices

- A very light-weight solution for testing React components, meant as a replacement for Enzyme
- Emphasizes testing components as a whole (rather than individual hooks/functions inside a component)
- Cheatsheet available [here](https://testing-library.com/docs/react-testing-library/cheatsheet)
- Read more [here](https://testing-library.com/docs/react-testing-library/intro)

#### [Redux Mock Store](https://github.com/dmitry-zaets/redux-mock-store)
> A mock store for testing Redux async action creators and middleware

