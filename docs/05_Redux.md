## Redux

This repo uses [Redux Toolkit](https://redux-toolkit.js.org/) (as recommended by the creators of Redux) because it greatly simplifies some of the more complex parts of working with Redux.


---


#### Contents

1. [What is Redux Toolkit?](#what-is-redux-toolkit)
2. [The Redux Store](#the-redux-store)
3. [The Root Reducer](#the-root-reducer)
4. [What Are Slices?](#what-are-slices)
5. [Generating Slices with NPM Scripts](#generating-slices-with-npm-scripts)
6. [Connecting to Redux](#connecting-to-redux)
7. [Testing](#testing)


___


#### What is Redux Toolkit?

At a high level, RTK

- aims to make the "hard" parts of using Redux "easy"
- simplifies configuring the global store (see [Simplifying Store Setup with configureStore](https://redux-toolkit.js.org/usage/usage-guide#simplifying-store-setup-with-configurestore))
- simplifies creating actions (see [Defining Action Creators with createAction](https://redux-toolkit.js.org/usage/usage-guide#defining-action-creators-with-createaction))
- simplifies creating reducers (see [Simplifying Reducers with createReducer](https://redux-toolkit.js.org/usage/usage-guide#simplifying-reducers-with-createreducer))
- enables us to create state "slices" (see [Simplifying Slices with createSlice](https://redux-toolkit.js.org/usage/usage-guide#simplifying-slices-with-createslice))
- comes with middleware (like [Redux Thunk](https://github.com/reduxjs/redux-thunk)) and tooling (like [Immer](https://immerjs.github.io/immer/docs/introduction)) built-in


___


#### The Redux Store

The Redux store is a SINGLETON - one global state is shared across the entire app.

This allows us to share cached data across pages, which improves UX speed and decreases the number of XHRs fired.

Setting up the store is as easy as
```
# @state/store

import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './reducer';

const store = configureStore({
    reducer: rootReducer,
});

export default store;
```


___


#### The Root Reducer

The root reducer defines the top-most level of the Redux state.

In this repo, we use one file to define:

- the root reducer itself
- the default root state
- the type definition for the root state

```
# @state/reducer

import { combineReducers } from '@reduxjs/toolkit';

// Slices
import usersReducer, { defaultState as defaultUsersState } from './users';
import { IState as IUsersState } from './users/types';

// Root State Type Def
export type IRootState = {
    users: IUsersState;
};

// Default State
export const defaultState = {
    users: defaultUsersState,
};

// Root Reducer
export default combineReducers({
    users: usersReducer,
});

```
___


#### What Are Slices?

Redux Toolkit encourages the simplification of all the actions/reducers setup by offering a new way to define/think about state management: "slices".

With slices, a single file is used to manage everything that deals with a "slice" of the global state.

**Example**
```
# @state/users

import { createSlice } from '@reduxjs/toolkit';
import { IThunkAction } from '@state/types';
import * as api from '@services/api';

export const defaultState = {
    data: [],
    fetching: false,
    fetchSuccess: false,
    fetchError: null,
};

const slice = createSlice({
    name: 'users',
    initialState: defaultState,
    reducers: {
        fetchRequest(state) {
            state.fetching = true;
            state.fetchSuccess = false;
            state.fetchError = null;
        },
        fetchSuccess(state, action) {
            state.data = action.payload;
            state.fetching = false;
            state.fetchSuccess = true;
            state.fetchError = null;
        },
        fetchError(state, action) {
            state.fetching = false;
            state.fetchSuccess = false;
            state.fetchError = action.payload;
        },
    },
});

export const { fetchRequest, fetchSuccess, fetchError } = slice.actions;
export default slice.reducer;

/* Thunk Actions */
export const fetchUsers = (): IThunkAction => async (dispatch, getState) => {
    const state = getState();

    // Check for current request
    if (state.users.fetching) return;

    // Check for previous request
    if (state.users.fetchSuccess) return;
    if (state.users.fetchError) return;

    // Issue new request
    dispatch(fetchRequest());
    try {
        const res = await api.getUsers();
        dispatch(fetchSuccess(res.data));
    } catch (e) {
        dispatch(fetchError(e.message));
    }
};

```

**Note:**

- A slice object returns **`actions`** and **`reducer`** properties.
- A slice file exports the reducer as default, and actions as named exports.
- Thunk actions must be defined separately.
- All actions (including Thunks) are bound to **`store.dispatch`** using [**`bindActionCreators`**](https://redux.js.org/api/bindactioncreators/) automatically.
- These standards are defined in the Redux Toolkit docs (see [createSlice Documentation](https://redux-toolkit.js.org/api/createSlice)).

___


#### Generating Slices with NPM Scripts

To save developers time & headache, this repo makes it simple to add new slices to the global state.

Run **`npm run generate state [sliceName]`** to

- generate files in the **`@state`** directory
- automatically update the root reducer (**`@state/reducer`**) by
    - adding new slice type definition to root state type definition
    - adding new slice default data to root state default data
    - adding new slice reducer to root reducer

Run **`npm run help generate`** for more information about generating slices.


___


#### Connecting to Redux

Mapping state/dispatch props to a React component is very simple using Redux Toolkit:

```
# @pages/UsersPage

import React from 'react';
import { connect } from 'react-redux';
import { IRootState } from '@state/types';
import { fetchUsers } from '@state/users';

interface IProps {
    users: IRootState['users'];
    fetchUsers: () => void;
}

const UsersPage = (props: IProps) => {
    React.useEffect(() => {
        if (props.users.data.length) return;
        if (props.users.fetching) return;
        if (props.users.fetchSuccess) return;
        if (props.users.fetchError) return;
        props.fetchUsers();
    }, [props.users]);
    return (
        <div>
            Total Users: {props.users.data.length}
        </div>
    );
};

const mapState = (state: IRootState) => ({
    users: state.users,
});

const mapDispatch = { fetchUsers };

export default connect(mapState, mapDispatch)(UsersPage);

```

**Note:**

- We map the entire slice to props in **`mapState`** (but can also use [**`reselect`**](https://github.com/reduxjs/reselect#readme), which is built in)
- Because **`store.dispatch`** is bound automatically to our actions (bindActionCreators), we are able to pass in an object literal (shorthand) for **`mapDispatch`**
    - Learn more about this [here](https://redux-toolkit.js.org/tutorials/intermediate-tutorial/#updating-the-add-todo-component)

___


#### Testing

Redux Toolkit + [Redux Mock Store](https://github.com/dmitry-zaets/redux-mock-store) make testing Redux extremely simple.

Learn more about how to test Redux slices in the [Testing Docs](/docs/08_Testing.md).

