## Path Aliases

Path aliases allow us to use semantic paths, rather than relative paths, whenever we use **`import`**.


---


#### Pros
- When folders get restructured or moved around, you don't have to fix broken relative paths.
- Allows developers to think about "what", instead of "where".

#### Cons
- Since there are several tools that use these aliases, their respective configurations each need to be updated.
    - **`./gatsby-config.js`** - Gatsby config, used to compile build + dev server
    -  **`./package.json`** - Jest config, used for unit testing
    - **`./tsconfig.json`** - TypeScript config, used for type-checking + ESLint


---


#### NPM Scripts

**View Aliases**

Run **`npm run alias`** to show a list of existing aliases.

**Manage Aliases**

Run **`npm run generate alias [aliasPath] [relativePath]`** to add or update an alias. This command will keep all the necessary configurations in sync so you don't have to.

Run **`npm run help generate`** for more information.

---


#### Example:

**Folder Structure**
```
.
├── gatsby-config.js
├── package.json
├── tsconfig.json
└── src
    └── components
        ├── forms
        |   └── LoginForm
        |       └── index.tsx
        ├── layouts
        |   └── Layout
        |       └── index.tsx
        └── pages
            └── LoginPage
                └── index.tsx
```

**Login Page**
> ./src/components/pages/LoginPage/index.tsx

```
// Relative Paths
import Layout from '../../layouts/Layout';
import LoginForm from '../../forms/LoginForm';

// Alias Paths
import Layout from '@layouts/Layout';
import LoginForm from '@forms/LoginForm';

const LoginPage = () => (
    <Layout>
        Log In
        <LoginForm />
    </Layout>
);

export default LoginPage;
```

**Gatsby Configuration**
> ./gatsby-config.js

```
module.exports = {
    plugins: [
        {
            resolve: `gatsby-plugin-alias-imports`,
            options: {
                alias: {
                    '@components': 'src/components',
                    '@forms': 'src/components/forms',
                    '@layouts': 'src/components/layouts',
                    '@pages': 'src/components/pages',
                },
                extensions: [],
            },
        },
    ]
}
```

**Jest Configuration**
> ./package.json

```
{
    "jest": {
        "moduleNameMapper": {
          "^@components(.*)$": "<rootDir>/src/components$1",
          "^@forms(.*)$": "<rootDir>/src/components/forms$1",
          "^@layouts(.*)$": "<rootDir>/src/components/layouts$1",
          "^@pages(.*)$": "<rootDir>/src/components/pages$1",
        }
    }
}
```

**TypeScript Configuration**
> ./tsconfig.json

```
{
    "compilerOptions": {
        "paths": {
            "@components/*": ["src/components/*"],
            "@forms/*": ["src/components/forms/*"],
            "@layouts/*": ["src/components/layouts/*"],
            "@pages/*": ["src/components/pages/*"],
        },
    }
}
```
