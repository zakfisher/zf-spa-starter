import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { IRootState } from '@state/types';

export interface IAccess {
    public?: boolean;
}

export interface IProps {
    app: IRootState['app'];
    children: React.ReactNode;
    title: string;
    access: IAccess;
    onLoad?: () => void;
}

const Page = ({ app, title, children, access, onLoad }: IProps) => {
    React.useEffect(() => {
        if (!app.ready) return;
        if (!onLoad) return;
        onLoad();
    }, [app.ready]);

    const showLoader = !app.ready && !_.get(access, 'public', false);

    return (
        <>
            <Helmet>
                <title>
                    {process.env.APP_NAME} - {title}
                </title>
            </Helmet>
            {showLoader ? <p>Loading...</p> : <>{children}</>}
        </>
    );
};
Page.displayName = 'Page';

const mapStateToProps = (state: IRootState) => ({
    app: state.app,
});

export default connect(mapStateToProps)(Page);
