import React from 'react';
import Page, { IProps as IPageProps } from '@pages/Page';

const pageProps = {
    title: 'Home',
    access: {
        public: true,
    },
} as IPageProps;

const HomePage = () => <Page {...pageProps}>HomePage</Page>;
HomePage.displayName = 'HomePage';

export default HomePage;
