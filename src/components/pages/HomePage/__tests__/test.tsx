import React from 'react';

import { render } from '@services/test/setup';
import HomePage from '..';

describe('HomePage', () => {
    it('renders correctly', () => {
        const { asFragment } = render(<HomePage />);
        expect(asFragment()).toMatchSnapshot();
    });
});
