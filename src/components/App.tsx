import React from 'react';
import { Provider as StoreProvider } from 'react-redux';
import store from '@state/store';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ITheme } from '@themes/types';
import { Helmet } from 'react-helmet';

interface IProps {
    element: React.ReactNode;
}

const THEMES: { [name: string]: ITheme } = {};
THEMES['default'] = require('@themes/default.ts').default;

const AdSense = () => (
    <Helmet>
        <script
            data-ad-client="ca-pub-5604392701701134"
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
        ></script>
    </Helmet>
);

const App = ({ element }: IProps) => {
    const theme: ITheme = THEMES[store.getState().app.theme];
    return (
        <StoreProvider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                {element}
                <AdSense />
            </ThemeProvider>
        </StoreProvider>
    );
};
App.displayName = 'App';
export default App;
