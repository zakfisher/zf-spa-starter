import { createSlice } from '@reduxjs/toolkit';
import { IThunkAction } from '@state/types';
// import * as api from '@services/api';

export const defaultState = {
    ready: false,
    theme: 'default',
};

const slice = createSlice({
    name: 'app',
    initialState: defaultState,
    reducers: {
        ready(state) {
            state.ready = true;
        },
        setTheme(state, action) {
            state.theme = action.payload;
        },
    },
});

export const { ready, setTheme } = slice.actions;
export default slice.reducer;

/* Thunk Actions */
export const initApp = (): IThunkAction => async dispatch => {
    // await api.helloWorld();

    // Load the page
    dispatch(ready());
};
