import { store } from '@services/test/setup';
import reducer, { defaultState, ready, initApp } from '.';

describe('app', () => {
    describe('defaultState', () => {
        it('should have expected data', () => {
            expect(defaultState.ready).toEqual(false);
        });
    });
    describe('actions', () => {
        describe('ready', () => {
            it('should dispatch the correct action', () => {
                expect(store.getActions()).toHaveLength(0);
                store.dispatch(ready());
                expect(store.getActions()[0].type).toBe('app/ready');
            });
        });
        describe('initApp', () => {
            it('should dispatch ready action', () => {
                expect(store.getActions()).toHaveLength(0);
                store.dispatch(initApp() as any);
                expect(store.getActions()[0].type).toBe('app/ready');
            });
        });
    });
    describe('state', () => {
        describe('ready', () => {
            it('should return the correct value', () => {
                expect(
                    reducer(defaultState, { type: 'any' }).ready
                ).toBeFalsy();
                expect(
                    reducer(defaultState, { type: 'app/ready' }).ready
                ).toBeTruthy();
            });
        });
    });
});
