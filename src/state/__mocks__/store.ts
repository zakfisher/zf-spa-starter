import configureStore, { MockStore, MockStoreCreator } from 'redux-mock-store';
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import { defaultState } from '../reducer';

const initialState = {
    ...defaultState,
    app: {
        ...defaultState.app,
        ready: true,
    },
};

const middleware = [...getDefaultMiddleware()];
const mockStore: MockStoreCreator = configureStore(middleware);
const store: MockStore = mockStore(initialState);

export default store;
