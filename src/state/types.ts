import { ThunkDispatch, ThunkAction } from 'redux-thunk';
import { IRootState } from './reducer';
export { IRootState } from './reducer';
export type IDispatch = ThunkDispatch<IRootState, {}, any>;
export type IThunkAction = ThunkAction<any, IRootState, null, any>;
