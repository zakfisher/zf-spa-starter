import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';

const config: AxiosRequestConfig = {
    baseURL: process.env.API_BASE_URL,
};

const api: AxiosInstance = axios.create(config);

export const getData = async () => ({
    data: [{ id: '134' }],
});

export const helloWorld = async () => await api.get('/hello_world');
