import React from 'react';
import '@testing-library/jest-dom/extend-expect';
require('dotenv').config({ path: '.env.test' });
import './mocks';

/* Export Redux Mock Store */
import mockStore from '@state/__mocks__/store';
export const store = mockStore;

/* Export React Render Method */
import App from '@components/App';
import { render as renderComponent } from '@testing-library/react';
export const render = (element: React.ReactNode) => {
    return renderComponent(<App element={element} />);
};
