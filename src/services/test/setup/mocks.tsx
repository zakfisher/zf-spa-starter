import React from 'react';

/* Mock i18n */
jest.mock('gatsby-plugin-intl', () => {
    const realIntl = require.requireActual('gatsby-plugin-intl');
    const intl = realIntl.createIntl({
        locale: 'en-US',
        messages: require('@i18n/en-US.json'),
    });
    const Link = ({
        to,
        children,
    }: {
        to: string;
        children: React.ReactNode;
    }) => <a href={to}>{children}</a>;
    Link.displayName = 'Link';
    return {
        ...realIntl,
        useIntl: () => intl,
        Link,
    };
});

/* Mock Redux Store */
jest.mock('@state/store');
import mockStore from '@state/__mocks__/store';
import { cleanup } from '@testing-library/react';

afterEach(() => {
    cleanup();
    mockStore.clearActions();
});

/* Mock Axios */
jest.genMockFromModule('axios');
