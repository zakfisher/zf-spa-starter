import { createMuiTheme } from '@material-ui/core/styles';
import { ITheme, IThemeConfig } from '@themes/types';

export const isTouchScreen =
    typeof window !== 'undefined' && 'ontouchstart' in window;

export const config: IThemeConfig = {
    mixins: {
        ellipsis: (width: string | number = '100%') => ({
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            width,
        }),
        hover: (style: any) => (isTouchScreen ? {} : style),
    },
    vars: {
        logoHeight: 70,
        logoWidth: 168,
    },
};

const theme: ITheme = createMuiTheme(config);
export default theme;
