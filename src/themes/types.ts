import { Theme } from '@material-ui/core/styles';
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme';

export type ITheme = Theme;

export type IThemeConfig = ThemeOptions & {
    mixins: any;
    vars: any;
};
